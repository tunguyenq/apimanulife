import * as Mongoose from "mongoose";
interface IPayloadUpdate {
  CurrentCallSale: number;
  CurrentMetting: number;
  CurrentContract: number;
  CurrentPresentation: number;
}

export { IPayloadUpdate };